from django.db import models
from django.utils.translation import gettext as _
from django.contrib.postgres.fields import JSONField


class DomainSearched(models.Model):
    domain_name = models.CharField(_("Searched Domain"), max_length=50)
    date_searched = models.DateTimeField(_("Date Searched"), auto_now=True)
    domain_data = JSONField(null=True, blank=True) 
    is_registered = models.BooleanField(_("Domain is registered"), default=False)
    domain_search_count = models.IntegerField(_("How many times a domain is searched"), null=True, blank=True, default=0)
    domain_image = models.FileField(_("Domain Image"), upload_to=f'domain-image/{domain_name}', max_length=100, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.domain_name, self.is_registered)

    class Meta:
        verbose_name = 'Domain Searched'
        verbose_name_plural = 'Domains Searched'