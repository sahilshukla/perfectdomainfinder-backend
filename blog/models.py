from django.db import models
from ckeditor.fields import RichTextField
from django.utils.translation import gettext as _
from django.utils import timezone

class Category(models.Model):
    name = models.CharField(_("Category Name"), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Series(models.Model):
    name = models.CharField(_("Series Name"), max_length=255)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Series"
        verbose_name_plural = "Series"


class Comment(models.Model):
    name = models.CharField(_("Commenter's name"), max_length=255)
    email = models.EmailField(_("Commenters Email"),)
    comment = RichTextField(_("Comment"),)
    blog = models.ForeignKey('blog', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"


class Blog(models.Model):
    title = models.CharField(_("Blog Title"), max_length=255)
    mast_head = models.FileField(_("Mast Head"), upload_to='blog/mast-head', max_length=255, null=True, blank=True)
    meta = RichTextField(_("Blog Summary"))
    blog = RichTextField(_("Blog"))
    date_created = models.DateTimeField(_("Date Created"), auto_now=True)
    date_updated = models.DateTimeField(_("Date Updated"), auto_now_add=True)
    date_published = models.DateTimeField(_("Date Published"), null=True, blank=True)
    series = models.ManyToManyField(Series, related_name='toseries')
    categories = models.ManyToManyField(Category, related_name='tocategory')
    published = models.BooleanField(_("Is Published"), null=True, blank=True, default=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.published:
            self.date_published = timezone.now()
        super(Blog, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Blog"
        verbose_name_plural = "Blogs"