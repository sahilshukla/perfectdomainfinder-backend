from django.conf import settings

class CorsMiddleware(object):
    def __init__(self,get_response):
        self.get_response=get_response

    def __call__(self, request):
        response=self.get_response(request)
        response["Access-Control-Allow-Origin"] = settings.CORS_ORIGIN_WHITELIST[0]
        return response 