from django.shortcuts import render
from blog.models import Blog, Series, Category, Comment
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from django.contrib.auth.decorators import login_required
from api.serializers import (BlogModelSerializer, CommentModelSerializer, SeriesModelSerializer, CategoryModelSerializer,
                             DomainSearchedModelSerializer)
from rest_framework.permissions import AllowAny, IsAuthenticated
from domainfinder.models import DomainSearched
import whois
from rest_framework.response import Response
from rest_framework import status
import json
from django.db.models import F
import datetime


class BlogModelViewSet(ModelViewSet):
    serializer_class = BlogModelSerializer
    queryset = Blog.objects.all()
    permission_classes = [AllowAny, ]


class CommentModelViewSet(ModelViewSet):
    serializer_class = CommentModelSerializer
    queryset = Comment.objects.all()
    permission_classes = [AllowAny, ]


class CategoryModelViewSet(ModelViewSet):
    serializer_class = CategoryModelSerializer
    queryset = Category.objects.all()
    permission_classes = [AllowAny, ]


class SeriesModelViewSet(ModelViewSet):
    serializer_class = SeriesModelSerializer
    queryset = Series.objects.all()
    permission_classes = [AllowAny, ]


class DomainSearchedModelViewSet(ModelViewSet):
    serializer_class = DomainSearchedModelSerializer
    queryset = DomainSearched.objects.none()
    permission_classes = [AllowAny, ]
    http_method_names = ['post', 'option', 'head']


class GetDomainDetail(APIView):
    def post(self, request):
        """
            Create an entry for DomainSearched based on every unique domain searched by user,
            if searched_domain already exists in the DB, increment the search_count by 1.
            If the domain is registered save the domain data in the concerned entry otherwise
            leave it empty! Also update `is_registered` if the whois-data is not empty.
        """
        searched_domain = request.data.get('domain')
        domain_searched, created = DomainSearched.objects.get_or_create(domain_name=searched_domain)

        if created:
            domain_searched.domain_search_count = 1
            domain_searched.is_registered = True
            domain_searched.save()
            try:
                whois_data = str(whois.whois(searched_domain))
                domain_searched.domain_data = json.dumps(whois_data)
                domain_searched.save()
                return Response(whois_data, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({'success': 'Great News! The domain is available.'}, status=status.HTTP_200_OK)
        else:
            domain_searched.domain_search_count = domain_searched.domain_search_count + 1
            domain_searched.save()
            serialized_data = DomainSearchedModelSerializer(domain_searched)
            return Response(serialized_data.data, status=status.HTTP_200_OK)