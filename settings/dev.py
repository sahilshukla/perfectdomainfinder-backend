from .base import *

ALLOWED_HOSTS = ['*']

CORS_ORIGIN_ALLOW_ALL = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'perfectdomainfinderbackend',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
    }
}