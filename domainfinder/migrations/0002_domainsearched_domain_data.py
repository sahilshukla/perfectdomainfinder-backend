# Generated by Django 3.0.4 on 2020-03-31 06:34

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('domainfinder', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='domainsearched',
            name='domain_data',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
    ]
