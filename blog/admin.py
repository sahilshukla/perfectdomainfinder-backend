from django.contrib import admin
from blog.models import *


@admin.register(Blog)
class Admin(admin.ModelAdmin):
    horizontal_filter = ['series', 'categories']
    fields = ['title', 'mast_head', 'meta', 'blog', 'series', 'categories', 'published', 'date_created', 'date_published', 'date_updated']
    readonly_fields = ['date_created', 'date_updated', 'date_published']
    


admin.site.register(Category)
admin.site.register(Series)
admin.site.register(Comment)
