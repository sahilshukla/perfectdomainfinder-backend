# Generated by Django 3.0.4 on 2020-04-01 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domainfinder', '0005_auto_20200331_0737'),
    ]

    operations = [
        migrations.AddField(
            model_name='domainsearched',
            name='domain_image',
            field=models.FileField(blank=True, null=True, upload_to='domain-image/<django.db.models.fields.CharField>', verbose_name='Domain Image'),
        ),
    ]
